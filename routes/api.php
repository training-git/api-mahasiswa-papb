<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('mahasiswa')->group(function(){
    Route::get('', [MahasiswaController::class, 'index']);
    Route::post('', [MahasiswaController::class, 'store']);
    Route::put('', [MahasiswaController::class, 'update']);
    Route::delete('', [MahasiswaController::class, 'delete']);
});