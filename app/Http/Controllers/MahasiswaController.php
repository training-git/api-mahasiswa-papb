<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;

class MahasiswaController extends Controller
{
    public function index(Request $req)
    {
        if($req == null) {
            $data = Mahasiswa::all();
            return response()->json([
                'status' => 'success',
                'mahasiswa' => $data
            ],200);
        } else if($req->has('id')) {
            $data = Mahasiswa::find($req->id);
            return response()->json([
                'status' => 'success',
                'mahasiswa' => $data
            ],200);
        } else {
            $data = Mahasiswa::where('nrp', 'LIKE', '%'. $req->nrp. '%')->get();
            return response()->json([
                'status' => 'success',
                'mahasiswa' => $data
            ],200);
        }
    }

    public function store(Request $req)
    {
        $data = new Mahasiswa;
        $data->nrp = $req->nrp;
        $data->nama = $req->nama;
        $data->email = $req->email;
        $data->jurusan = $req->jurusan;
        $data->save();

        return response()->json([
            'status' => 'success',
            'mahasiswa' => $data
        ],200);
    }

    public function update(Request $req)
    {
        $data = Mahasiswa::find($req->id);
        $data->nrp = $req->nrp;
        $data->nama = $req->nama;
        $data->email = $req->email;
        $data->jurusan = $req->jurusan;
        $data->save();

        return response()->json([
            'status' => 'success',
            'mahasiswa' => $data
        ],200);
    }

    public function delete(Request $req)
    {
        $data = Mahasiswa::find($req->id);
        $data->delete();

        return response()->json([
            'status' => 'success',
            'mahasiswa' => $data
        ],200);
    }
}
